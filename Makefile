CC=gcc
ASMBIN=nasm
LIB=-lGLU -lGL -lglut -lm -lstdc++

all : asm cc link

asm : func.asm
	$(ASMBIN) -g -o func.o -f elf64 -l func.lst func.asm
cc : main.cpp
	$(CC) -c -g3 -D_DEBUG -O0 main.cpp
link : cc asm
	$(CC) -o intel ${LIB} main.o func.o
clean :
	rm *.o
	rm intel
	rm func.lst
	