; kate:tab-width 8;
;
;
;	(RDI, 	RSI, 	RDX,	RCX,	R8,	R9	) | (XMM0–7)
;	(in,	out,	mask,	size,	w	-	)
;
;	

section	.text
global  func

func:
	push	RBP
	mov	RBP, 		RSP
	
; 	sub	RCX,		R8
;  	sub	RCX,		4
	
	mov	R10,		RDI
	add	R10,		R8
	mov	R11,		R10
	add	R11,		R8	
	
; 	mov	R14,		R10
; 	mov	R15,		R11
	
	;laduj maske
	movups	XMM0, 		[RDX]		;3 istotne wartosci w x3, x2, x1
	movups	XMM1,		[RDX+12]
	movups	XMM2,		[RDX+24]
	
	movups	XMM3,		[RDI]
	movups	XMM4,		[RDI+24]
	shufps	XMM3,		XMM3,		11111100b ; [x0][x3][x3][x3]
	shufps	XMM4,		XMM4,		11111100b
	shufpd	XMM3,		XMM4,		0000b
	
	movups	XMM4,		[R10]
	movups	XMM5,		[R10+24]
	shufps	XMM4,		XMM4,		11111100b
	shufps	XMM5,		XMM5,		11111100b
	shufpd	XMM4,		XMM5,		0000b
	
	movups	XMM5,		[R11] 
	movups	XMM6,		[R11+24]
	shufps	XMM5,		XMM5,		11111100b
	shufps	XMM6,		XMM6,		11111100b
	shufpd	XMM5,		XMM6,		0000b
	
	add	RSI,		R8
	add	RSI,		12
	
; 	mov	R13,		RSI
	
mainlo1:	
	; main compute ---------------------------------------------------------;
	movaps	XMM6,		XMM3                                            ;
	movaps	XMM7,		XMM4                                            ;
	movaps	XMM8,		XMM5                                            ;
	                                                                        ;
	mulps	XMM6,		XMM0 ;                                          ;
	mulps	XMM7,		XMM1 ;                                          ;
	mulps	XMM8,		XMM2 ;                                          ;
	                                                                        ;
	addps	XMM6,		XMM7                                            ;
	addps	XMM6,		XMM8                                            ;
	                                                                        ;
	shufps	XMM6,		XMM6,		00100100b ; [x0][x1][x2][x0]    ;
	pshufd	XMM7,		XMM6,		01010101b ; [x1][x1][x1][x1]    ;
	movhlps	XMM8,		XMM6			  ; [x2][x3][-old--]    ;
	                                                                        ;
	addps	XMM6,		XMM7                                            ;
	addps	XMM6,		XMM8                                            ;
	                                                                        ;
	movss	[RSI],		XMM6                                            ;
	add	RSI,		12                                              ;
	                                                                        ;
	;-----------------------------------------------------------------------;                                                                        
	                                                                        
	add	RDI,		24 ; try 12
	add	R10,		24 ; try 12
	add	R11,		24 ; try 12
	
	shufps	XMM3,		XMM3,		00111001b	; dword-shift left
	shufps	XMM4,		XMM4,		00111001b
	shufps	XMM5,		XMM5,		00111001b
	
	; main compute ---------------------------------------------------------;
	movaps	XMM6,		XMM3                                            ;
	movaps	XMM7,		XMM4                                            ;
	movaps	XMM8,		XMM5                                            ;
	                                                                        ;
	mulps	XMM6,		XMM0 ;                                          ;
	mulps	XMM7,		XMM1 ;                                          ;
	mulps	XMM8,		XMM2 ;                                          ;
	                                                                        ;
	addps	XMM6,		XMM7                                            ;
	addps	XMM6,		XMM8                                            ;
	                                                                        ;
	shufps	XMM6,		XMM6,		00100100b ; [x0][x1][x2][x0]    ;
	pshufd	XMM7,		XMM6,		01010101b ; [x1][x1][x1][x1]    ;
	movhlps	XMM8,		XMM6			  ; [x2][x3][-old--]    ;
	                                                                        ;
	addps	XMM6,		XMM7                                            ;
	addps	XMM6,		XMM8                                            ;
	                                                                        ;
	movss	[RSI],		XMM6                                            ;
	add	RSI,		12                                              ;
	                                                                        ;
	;-----------------------------------------------------------------------;
	
	shufps	XMM3,		XMM3,		00111001b
	movups	XMM4,		[RDI]
	shufps	XMM4,		XMM4,		11111100b
	shufpd	XMM3,		XMM4,		0000b
	shufpd	XMM3,		XMM3,		0001b
	
	shufps	XMM4,		XMM4,		00111001b
	movups	XMM5,		[R10]
	shufps	XMM5,		XMM5,		11111100b
	shufpd	XMM4,		XMM5,		0000b
	shufpd	XMM4,		XMM4,		0001b
	
	shufps	XMM5,		XMM5,		00111001b
	movups	XMM6,		[R11]
	shufps	XMM6,		XMM6,		11111100b
	shufpd	XMM5,		XMM6,		0000b
	shufpd	XMM5,		XMM5,		0001b
	
	dec	ECX
	JNZ	mainlo1
	

	pop	RBP
	ret
	