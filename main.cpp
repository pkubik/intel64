/*
 * Paweł Kubik
 * ARKO
 * Projekt Intel 
 * 
// 	Przedmiot: GKOM lab.
// 	Cwiczenie: 3
// 	Temat: Przyklad programowania z uzyciem biblioteki OpenGL
// 	Obracajacy sie szescian.
// 	Autor: Zbigniew Szymanski na podstawie szkieletu
// 	programu autorstwa Briana Paula
// 	Data: 24.03.2000
*/

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <GL/glut.h>
#include <cstring>

// float mask1[10] = { //10 zeby movaps bezpiecznie wczytal ostatnia linie
// 	1.,	2.,	1.,
// 	2.,	4., 2.,
// 	1.,	2.,	1.
// };

float mask1[10] = { //10 zeby movaps bezpiecznie wczytal ostatnia linie
	1.,	1.,	1.,
	1.,	0.1, 1.,
	1.,	1.,	1.
};

// float mask2[10] = {
// 	0,	-2,	0,
// 	-2,	11,	-2,
// 	0,	-2,	0
// };

float mask2[10] = {
	-1,	-1,	-1,
	-1,	8,	-1,
	-1,	-1,	-1
};

float diffmask[10];

float filtr[10];
float* texture;
float* texbuf;
unsigned int imageSize;
int windowID;
GLuint textureID;
unsigned int texwidth  = 512;
unsigned int texheight = 512;

extern "C" void func(float* intex, float* outtex, float* mask, unsigned size, unsigned w);

static void normMasks() {
	float sum = 0;
	for(unsigned i=0;i<9;++i) {
		sum += mask1[i];
	}
	if(sum) {
		for(unsigned i=0;i<9;++i) {
			mask1[i] /= sum;
		}
	}
	
	sum = 0;
	for(unsigned i=0;i<9;++i) {
		sum += mask2[i];
	}
	if(sum) {
		for(unsigned i=0;i<9;++i) {
			mask2[i] /= sum;
		}
	}
	
	for(unsigned i=0;i<9;++i) {
		diffmask[i] = (mask2[i]-mask1[i])/(256*256); //arbitralna liczba stanow posrednich
	}
}

GLuint loadBMP(const char *imagepath, float*& texdata) {
	unsigned char header[54];
	unsigned int dataPos;
	unsigned char * data;
	
	FILE * file = fopen(imagepath,"rb");
	if (!file) {
		printf("Image could not be opened\n");
		return 0;
	}
	
	if ( fread(header, 1, 54, file)!=54 ) {
		printf("Not a correct BMP file\n");
		return false;
	}
	
	if ( header[0]!='B' || header[1]!='M' ){
		printf("Not a correct BMP file\n");
		return 0;
	}
	
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	texwidth = *(int*)&(header[0x12]);
	texheight = *(int*)&(header[0x16]);
	
	if (imageSize==0)
		imageSize=texwidth*texheight*3;
	if (dataPos==0) 
		dataPos=54;
	
	// Create a buffer
	data = new unsigned char[imageSize];
	
	texdata = new float[imageSize];
	
	// Read the actual data from the file into the buffer
	fread(data,1,imageSize,file);
	
	//Everything is in memory now, the file can be closed
	fclose(file);
	
	GLuint textureID;
	glGenTextures(1, &textureID);
	
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);
	
	unsigned di = 0;
	unsigned tdi = 0;
	
	while(di!=imageSize) {
		texdata[tdi] = float(data[di])/0xff;
		++di;
		++tdi;
	}
	
	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texwidth, texheight, 0, GL_BGR, GL_FLOAT, texdata);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}

static void Quit() {
	glutDestroyWindow(windowID);
	delete texbuf;
	delete texture;
	glDeleteTextures(1, &textureID);
	exit(0);
}

static void Display( void )
{
	static bool bBusy=false;
				
	if (bBusy) return;
	
	bBusy=true;
					
	glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	glEnable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0);
	
	glPushMatrix();
	
	//tutaj rysowanie sceny
	glBegin(GL_QUADS);
	glTexCoord2d(0, 0); glVertex2f(-1.0, -1.0);
	glTexCoord2d(0, 1); glVertex2f(-1.0, 1.0);
	glTexCoord2d(1, 1); glVertex2f(1.0, 1.0);
	glTexCoord2d(1, 0); glVertex2f(1.0, -1.0);
	glEnd();
	glPopMatrix();
	glutSwapBuffers();
	bBusy=false;
}

static void Reshape( int width, int height )
{
	glViewport( 0, 0, width, height); //wsp. rzutni w oknie

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glFrustum( -1.0, 1.0, -1.0, 1.0, 0.0, 0.0 );

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	glTranslatef( 0.0, 0.0, 0.0 );
}

 //Obsluga klawiatury (znaki ASCII)
 //x,y - wspolrzedne polozenia myszy
static void Key( unsigned char key, int x, int y )
{
	if(key == 27) {
		Quit();
	}
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texwidth, texheight, GL_BGR, GL_FLOAT, texture);
	glutPostRedisplay();
}

static void SpecialKey( int key, int x, int y )
{
	if(key == GLUT_KEY_RIGHT)
		/**/;
	else if(key == GLUT_KEY_LEFT)
		/**/;
	
	
	//skomplikowane operacje na filtrach...
	memcpy(filtr, mask1, 9*4);
	
	func(texture, texbuf, filtr, texwidth*(texheight-2)/2-1, texwidth*12);
	func(texture+1, texbuf+1, filtr, texwidth*(texheight-2)/2-1, texwidth*12);
	func(texture+2, texbuf+2, filtr, texwidth*(texheight-2)/2-1, texwidth*12);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texwidth, texheight, GL_BGR, GL_FLOAT, texbuf);
	
	glutPostRedisplay();
}

static void MouseMove(int x, int y) {
	for(unsigned i=0;i<9;++i) {
		filtr[i] = mask1[i]+diffmask[i]*((x*y)>>2);
	}
	
	//memset(texbuf, x*y, imageSize*4);
	func(texture, texbuf, filtr, texwidth*(texheight-2)/2-1, texwidth*12);
	func(texture+1, texbuf+1, filtr, texwidth*(texheight-2)/2-1, texwidth*12);
	func(texture+2, texbuf+2, filtr, texwidth*(texheight-2)/2-1, texwidth*12);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, texwidth, texheight, GL_BGR, GL_FLOAT, texbuf);
	glutPostRedisplay();
}

static void Init(void)
{   
	glDisable(GL_DEPTH_TEST);	
	textureID = loadBMP("in.bmp", texture);
	texbuf = new float[imageSize];
	
	memset(texbuf, 0/*01111111*/, imageSize*4); //zabezpieczenie przed niewlasciwym floatem
	
	normMasks();
}


int main( int argc, char *argv[] )
{
	glutInit( &argc, argv );
	
	glutInitWindowPosition(-1, -1);
	
	glutInitWindowSize(512, 512);

	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);

	windowID = glutCreateWindow("Filtr");

	glutReshapeFunc( Reshape );

	glutKeyboardFunc( Key );

	glutSpecialFunc( SpecialKey );

	glutDisplayFunc( Display );
	
	glutPassiveMotionFunc( MouseMove );

	Init();
	
	glutMainLoop();
	return 0;
}

